package main

import (
	"github.com/tjsampson/chat-lacks/core"
	"github.com/tjsampson/chat-lacks/server"
)

func main() {
	log, file := core.CreateLogger(core.AppConfiguration.LogOutput)
	if file != nil {
		defer file.Close()
	}
	log.Panicln(server.ListenAndServe(log, core.AppConfiguration))
}
