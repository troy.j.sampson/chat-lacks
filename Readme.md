# Lacks Chat Server

## Overview

A chat server writen in Golang that supports TCP and HTTP connections.

## Usage

Clone the repo and ensure deps are installed:

```bash
dep ensure
```

then

```bash
go run main.go
```

or

```bash
make && ./chat-lacks
```

or

```bash
go build && ./chat-lacks
```

## Default Config

```yaml
TCPPort:   "3000"
HTTPPort:  "8000"
HostAddr:  "localhost"
LogOutput: "stdout"
```

### Chatroom Useage

Users can interact with the chatroom through TCP and/or HTTP connections.

#### TCP Connections

```bash
telnet localhost 3000
```

#### HTTP Connections

Http Connections use a cookie (Not secure) to maintain a user session.

```bash
curl localhost:4000/
```

or

```bash
curl --cookie "username=troy" -H "Content-Type: application/json" -X POST -d '{"user":"troy"}' localhost:4000
```

## Roadmap

- Finish up the HTTP Api and add in a webSocker endpoint
- Create a client side web app.
- More tests
